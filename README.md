# Omnifi Projects

This project contains a complete collection of proposed, ongoing, archived projects within the [Omnifi](https://omnifi.foundation) community. This collection is used to update the online content of [Omnifi Foundation](https://omnifi.foundation) and is considered complete. 

## Starting a project

There are several ways a project can be started at [Omnifi Foundation](https://omnifi.foundation), whilst critically each process must be driven by the mandated mission of [Omnifi Foundation](https://omnifi.foundation) and its community, which is predominently focused on human-centric issues including universal privacy, health, and sustainability through technology. 

### Proposals

The main entrypont to starting a project is through a [proposal](./proposed/_template.md). Proposals begin by creating a new folder in the **proposed** directory and adding a new markdown file with the name of the project, (in [snake case](https://en.wikipedia.org/wiki/Snake_case)), using the [template](./proposed/_template.md) provided. 

Alternatively if the proposal is an extension of an existing project or group of projects, then an existing folder can be used with a new markdown for the extension. This method also enables multiple related proposals to be submitted at the same time with the potential for partial acceptance. 

> **Note**: If a project has been moved further in the process, beyond the *proposed* stage, and the new proposal is an extension of that matured project, then a new folder will need to be created in **proposed** with the same project name to ensure integrity between initiatives.

Once a project has been submitted it will be reviewed by members with feedback provided, if necessary. It's almost always anticipated that feedback will be required to refine a proposal before a project is accepted, and this process is intended to bolster and expand on the initial concept, and support the project team.

If a project is accepted it will be moved to either **incubating** or **sanboxed** depending on the review. If eventually unsuccessful the project will be moved to **archived**. *Archived* projects can be renewed when either circumstances change, or the proposal is significantly reworked, and are kept for posterity and transparency. 

## Incubating

During incubation support will be provided by [Omnifi Foundation](https://omnifi.foundation) members to ensure a project has the best chance of success. This will include supporting development plans, arranging discussions, and feeding back into the progress of a project. If a project does not make significant progress towards its main goals during a reasonable period then it will be flagged with the core project team. 

If the project situation is not improved after two further flags then an option may be given for the project team to change and a new plan to be submitted. If this option is not undertaken then the project will be archived. 

In most cases we are more likely to extend the incubation period to support a project than to archive it. Our goals are to create stable ecosystems for projects and closing projects is never in the best interest of the community. In the worst case scenario we may reach out to other cooperatives and find new stewards for a project to ensure it continues rather than shut a project down entirely.

Once a project has reached maturity with a stable release and continued deliveries, then it will be *graduated* (and therefore moved to **graduated**). *Graduated* projects are mostly self-managed projects with occasional (_annual_) check-ins with the [Omnifi Foundation](https://omnifi.foundation) members to ensure it has the support needed to continue. 

Certain criteria may be specified before graduating a project, such as improving the quality of security, or the diversity of the project team, and these criteria will then be evaluated on a regular basis.

## Sandboxing

Projects may be sandboxed for a variety of reasons, starting with the possibility that they were intentionally proposed to be sandboxed in the first place. Sandboxing can be achieved immediately from the *proposal* stage and comes with several benefits over *incubation* and *graduation*. Essentially *sandboxed* projects are research and development projects intended to validate hypotheses and test the potential for specified technologies to be used. 

By sandboxing a project can have very simple goals, less oversight, and an ability to move quickly without being concerned about the end results. An additional benefit of sandboxing is in the ability for a project to be stagnant for longer without being archived. Many sandboxed projects are expected to be side or hobby projects, and therefore will be limited contributions. 

A *sandboxed* project can be promoted to either *incubation* or *graduation* in some rare cases, depending on the maturity of the project. It is often far better, and recommended, to create a *proposal* for a *sandboxed* project first to move it to *incubation* as a fully-fledged project. In some rare cases the [Omnifi Foundation](https://omnifi.foundation) members may elect to move it autonomously if the project shows promise and sufficient quality and traction. 

## Graduating

The main goal for most projects is to *graduate* in order to be considered a fully-fledged part of the [Omnifi Foundation](https://omnifi.foundation) community. *Graduate* projects will be supported with both additional time and potential funding with full-time project teams being a typical option. They will also be provided with sufficient infrastructure support, including email and domain hosting as independently identifiable projects. 

*Graduate* projects are also provided with additional protection to ensure they continue to operate, with [Omnifi Foundation](https://omnifi.foundation) taking responsibility to ensure the project is never shut down unnecessarily, or to the detriment of its community. 

## Sunsetting

A project can only be sunset if it has moved beyond the *proposal* phase, and may be *sunset* for a variety of reasons. In most cases [Omnifi Foundation](https://omnifi.foundation) will avoid sunsetting a project, with the exception of *sandboxed* projects which are conceived of as impermanent efforts by default. By sandboxing a project [Omnifi Foundation](https://omnifi.foundation) will enable the project to move outside of the organisation and community and, in most case, will support this to ensure a smooth transition. 

## Archiving

It is anticipated that most *archived* projects will have never moved beyond the *proposal* phase of the process. However, in some rare cases projects will be *archived* if they are considered unsuitable to continue. Where-as *sunset* projects are essentially archived projects intended to exist, without support, *archived* projects are intended to be stopped and will cease to have their assets published. This could happen in the case of patent or intellectual property issues, or other ethical issues that render a project unsustainable. 
